from django.shortcuts import render
from datetime import datetime, date

# Enter your name here
mhs_name1 = 'Sandika Prangga Putra'
curr_year = int(datetime.now().strftime("%Y"))
birth_date1 = date(1999, 4, 16)
npm1 = 1706984562
hobby1 = "Gaming"
desc1 = "Seorang yang biasa"

mhs_name2 = 'Bricen Sarido Simamora'
birth_date2 = date(1998, 1, 26)
npm2 = 1706043935
hobby2 = "Main voli"
desc2 = "Tinggi"

# Create your views here.
def index(request):
    response = {'name1': mhs_name1, 'age1': calculate_age(birth_date1.year), 'npm1': npm1,
    'hobby1': hobby1, 'desc1': desc1,
    'name2': mhs_name2, 'age2': calculate_age(birth_date2.year), 'npm2': npm2,
    'hobby2': hobby2, 'desc2': desc2}
    return render(request, 'index.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
